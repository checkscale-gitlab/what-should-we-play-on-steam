# [What Should We Play On Steam](https://whatshouldweplayonsteam.com/)
I had the issue that when a group of friends wanted to play a game together, but no-one could think of a game to play.
So we would start comparing games in our libraries, which is a slow and fun ruining task.
So I thought that it would be a fun project to build, and explore the [Steam APIs](https://developer.valvesoftware.com/wiki/).

## CI/CD
This is automatically build using GitLab CI and pushed to the projects [Container Registry](https://gitlab.com/Timmy1e/what-should-we-play-on-steam/container_registry).

Check out the [production branch](https://gitlab.com/Timmy1e/what-should-we-play-on-steam/-/tree/production) to see an example of how to deploy this application.
